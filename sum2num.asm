; this is a simple program in x86_64 to explain basic concept of assembly next is socket connections and then shellcodes
section .data
	text db "starting",10,0; declare it is starting
	len equ $-text ; found the lenght of text $ means len position . ie pos(len-text) that gives u lenght of string 
section .bss
	num1:	resb 64; reserving 64 but the program can only accept 2^8 input cause i use 8 bit registers
	num2:	resb 64
	final_num:	resb 64
section .text
global _start
_start:	
			mov rax,1;this part is to print starting look at syscalls for more info
			mov rdi,1
			mov rsi,text
			mov rdx,len
			syscall
			call main
			mov rax,60; exit 
			mov rdi,0	
			syscall
main:
			
			push rbp
			mov rbp,rsp
			mov rax,0
			mov rdi,0	
			mov rsi,num1;this part is to take input
			mov rdx,16; just took an arbitory number remember max is only 2**8 
			syscall
			lea rax,[num1]
			call multi1; this is to take those input numbers which are in ascii and to convert it to actuall numbers we can perform math on
			push rbx
			mov rax,0
			mov rdi,0	
			mov rsi,num2
			mov rdx,16
			syscall
			lea rax,[num2];rax should be an argument for multi so as to get these adress
			call multi1
			pop rax
			add rax,rbx; adding num1 and num2
			lea rbx,[final_num]


multi:		cmp rax,9
			push "$";starting symbol
			jge multiple
			push rax

back:		pop rax
			cmp rax,'$'
			jz done
			mov byte[rbx],al
			add byte[rbx],0x30;this is to convert all num to ascii for final answer
			inc rbx
			jmp back
done:		mov rax,1
			mov rdi,1
			mov rsi,final_num
			mov rdx,16
			syscall
			pop rbp
			ret
multiple:	
			mov rcx,10
			mov rdx,0
			div rcx
			push rdx
			cmp rax,1
			jge multiple
			jmp back


multi1:	push rbp
		mov rbp,rsp
		push '$';indicates starting symbol so as to know when to stop when u pop
		mov rbx,rax;rax is needed for mul so i transfered to to rbx u can work with rax just don't forget to clear when using mul
		mov rcx,0
check:	
		mov cl,byte[rbx]
		cmp cl,0x0a
		jz got
		sub cl,0x30; to remove ascii
		push rcx
		inc rbx
		jmp check
got:	
		mov rbx,0
		mov rax,0
		mov rcx,1
bak:	pop rax
		cmp al,'$'
		jz number
		mul rcx
		add bl,al
		mov rax,rcx
		mov rcx,10;following the rule 123=1*100+2*10+3*1
		mul rcx
		mov rcx,rax
		mov rax,0
		jmp bak
number:	pop rbp;result is stored in rbx
		ret