# x86_64 basic concept asm
This is a simple program to add two numbers in assembly x86_64 to get a feel for the language
sources: https://www.cs.uaf.edu/2006/fall/cs301/support/x86_64/
		 http://blog.rchapman.org/posts/Linux_System_Call_Table_for_x86_64/
-------------------------------compilation instruction---------------------------------
nasm -f elf64 sum2num.asm -o somename.o
ld somename.o -o somename1
